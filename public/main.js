jQuery(document).ready(function ($) {

    $('#exampleModal').on('show.bs.modal', function (event) {
        //event.preventDefault();

        let modal = $(this);
        let button = $(event.relatedTarget);
        modal.find('.modal-body').html(button.parents('td').find('.full-message').html());
    });

    // Add fields
    $('#account_added').on('click', function(event) {
    	event.preventDefault();
    	var div = $('.form-input:last');
    	var num = parseInt( div.prop("id").match(/\d+/g), 10 ) +1;
    	var duplicate = div.clone();
    	var input = duplicate.find('input');
    	input.each(function( item, index ) {
    		$(this).val("");
    		var id = $(this).prop('id').replace(/\d+/, num); 
    		var name = $(this).prop('name').replace(/\d+/, num);
    		
    		$(this).prop('id', id);
    		$(this).prop('name', name);
    	});
    	duplicate.prop('id', 'input'+num);
		div.after( duplicate );
		$('#account_delete').show();
	});
	
	$('#account_delete').on('click', function(event) {
		event.preventDefault();
		$('.form-input').last().remove();
		if( $('.form-input').length == 1 ) {
			$('#account_delete').hide();
		}
	});

    //Form request
    $('#login').submit( function(event) {
    	event.preventDefault();

    	var form = $(this);
    	var url = form.attr('action');

    	 $.ajax({
           	type: "POST",
           	url: url,
           	data: form.serialize(), 
           	success: function(data)
           	{
				if( !data ) return;
				var select = document.getElementById('testSelect');	
				
				$.each(JSON.parse(data), function(key, value) {	
								
					$.each(value, function(index,item) {
						var str = 'email';
						console.log(index);
						if( index.indexOf(str) != -1 ) {
							var opt = document.createElement('option');
							opt.value = item;
							opt.innerHTML = item;
							select.appendChild(opt);
						}
					});
				});
           	}
        });
    });
});