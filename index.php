<?php
require_once( './vendor/autoload.php' );
$mailbox = new \Imap\Imap();

if ( $mailbox->isAuth() ) {
	$data = $mailbox->getData();
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Test Work</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="/public/style.css">

</head>
<body style="color: #000">

<nav class="navbar navbar-light bg-light">
    <span class="navbar-brand mr-auto mb-0 h1">Hello! ;) : <?php echo $mailbox->getCurrentEmail() ?></span>
	<?php if ( $mailbox->isAuth() ) : ?>
        <div class="my-2 my-lg-0">
            <a href="/?logout=1" class="btn btn-outline-success my-2 my-sm-0" type="submit">Logout</a>
        </div>
	<?php endif ?>
</nav>

<div class="container">

    <div class="col-md-6 mx-auto text-center mt-5">
        <div class="header-title">
            <h2 class="wv-heading--subtitle">
                Test work of Gmail Imap
            </h2>
        </div>
    </div>

	<?php if ( ! $mailbox->isAuth() ) : ?>
        <!-- Login Form -->
        <div class="row">
            <div class="col-md-6 mx-auto">
                <div class="myform form ">
                    <form id="login" action="/inc/ImapHandler.php" method="post" name="login">
                        <div class="form_wrapper">
                        <div id="input1" class="form-group form-flex form-input">
                            <input type="email" name="email1" class="form-control my-input" id="email1"
                                   placeholder="Your email" required>
                        
                            <input type="password" name="pass1" class="form-control my-input" id="pass1"
                                   placeholder="Email password" required>
                        </div>
                        <div class="add-account">
                            <a id="account_added" class="btn send-button btn-info">+</a>
                            <a id="account_delete" class="btn send-button btn-info">-</a>
                        </div>
                        </div>
                        <div class="text-center ">
                            <button type="submit" class=" btn btn-block send-button btn-info">Log in</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End Login Form -->
        <div class="row">
            <div class="col-md-6"><select id="testSelect"></select></div>
        </div>
	<?php else : ?>
        <!-- Datatable -->

        <div class="row">
            <div class="col-12 my-3">
                <div class="row">
                    <form class="form-inline col-8" name="filters" method="get">
                        <label class="mr-sm-2" for="search">Search: </label>
                        <input id="search" type="text" name="search"
                               value="<?php echo ( $_GET['search'] ) ? $_GET['search'] : null ?>"
                               placeholder="Amazon..."
                               class="form-control mb-2 mr-sm-2 mb-sm-0"/>

                        <label class="mr-sm-2" for="sort">Sort: </label>
                        <select id="sort"
                                name="sort"
                                class="form-control custom-select mb-2 mr-sm-2 mb-sm-0"
                                title="sort">
                            <option value="asc">ASC</option>
                            <option value="desc" <?php echo ( $_GET['sort'] == 'desc' ) ? 'selected' : null ?>>DESC
                            </option>
                        </select>

                        <label class="mr-sm-2" for="count">Count: </label>
                        <select id="count"
                                name="count"
                                class="form-control custom-select mb-2 mr-sm-2 mb-sm-0"
                                title="count">
							<?php foreach ( [ '5', '10', '20', '30' ] as $item ) : ?>
                                <option value="<?php echo $item ?>" <?php echo ( isset( $_GET['count'] ) && $_GET['count'] == $item ) ? 'selected' : null ?>>
									<?php echo $item ?>
                                </option>
							<?php endforeach; ?>
                        </select>
                        <button id="btnSearch" type="submit" class="btn btn-success">Search</button>
                    </form>

                    <form class="form-inline col-4" name="write_file" method="post">
                        <input type="hidden" name="write" value="<?php echo $data['emails_ids'] ?>">
                        <button id="btnWriteFile" type="submit" class="btn btn-success">Write File</button>
                    </form>
                </div>

            </div>
        </div>


        <div class="row">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">From</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Date</th>
                    <th scope="col">Message</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>


				<?php
				if ( $data['data'] ): ?>
                    <tbody>
					<?php foreach ( $data['data'] as $i => $row ) : ?>
                        <tr>
                            <th scope="row"><?php echo $i + 1 ?></th>
                            <td><?php echo $row['from'] ?></td>
                            <td><?php echo $row['subject'] ?></td>
                            <td><?php echo $row['date'] ?></td>
                            <td>
								<?php echo $row['message'] ?>
                                <a data-toggle="modal" data-target="#exampleModal" class="show-more" href="#">
                                    Show more</a>
                                <div class="full-message d-none">
									<?php echo $row['full_message'] ?>
                                </div>
                            </td>
                            <td style="width: 15%">
                                <a href="/?del=<?php echo $row['id'] ?>" class="btn btn-danger">x</a>
                                <a href="/?rep=<?php echo $row['id'] ?>" class="btn btn-success">></a>
                            </td>
                        </tr>
					<?php endforeach; ?>

				<?php else : ?>
                    <p class="text-center">Mailbox is empty</p>
                    </tbody>
				<?php endif; ?>

            </table>
        </div>
        <!-- End Datatable -->
	<?php endif ?>

</div>


<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"
        integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>

<script src="/public/main.js"></script>

</body>
</html>

