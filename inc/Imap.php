<?php namespace Imap;

use PhpImap,
	Throwable,
	Error,
	Imap\ImapSession as ImapSession,
	Imap\ImapFile as ImapFile;

/**
 * Class Imap
 * @package Imap
 */
class Imap {

	/**
	 * @var PhpImap\Mailbox $imap
	 */
	private $imap;

	/**
	 * Session instance.
	 *
	 * @var $auth
	 */
	public $auth = null;

	/**
	 * @var \Imap\ImapSession
	 */
	private $session;

	/**
	 * Imap constructor.
	 *
	 * @param string|null $login
	 * @param string|null $pass
	 * @param array|null  $opts
	 */
	public function __construct( string $login = null, string $pass = null, array $opts = null ) {

		try {
			$this->checkEnvironment();

			$this->session = new ImapSession();

			if ( ! $this->session->getSession() && ! $this->setAuth( $login, $pass ) ) {
				throw new \Exception( 'some problems with credentials' );
			}

			$this->setImap();
			$this->initTriggers();

			return $this->getImap();

		} catch ( Throwable $e ) {
			echo $e->getMessage();
		}

		return false;
	}

	private function initTriggers() {
		//Logout
		if ( isset( $_GET['logout'] ) && $this->isAuth() ) {
			$this->logOut();
		}

		//Del email by id
		if ( isset( $_GET['del'] ) && ! empty( $_GET['del'] ) && $this->isAuth() ) {
			$this->deleteMessage( $_GET['del'] );
		}

		if ( isset( $_POST['write'] ) && $this->isAuth() ) {
			$file   = new ImapFile();
			$emails = $this->getEmailsByIds( explode( ',', $_POST['write'] ) );
			$file->writeFile( $emails );
		}
	}

	/**
	 * @return bool
	 */
	private function checkEnvironment() {
		if ( extension_loaded( 'imap' ) ) {
			return true;
		}
		throw new Error( 'The php_imap library error' );
	}

	/**
	 * Set Auth
	 */
	private function setAuth( $login = null, $pass = null ) {

		if ( ! empty( $_POST ) ) {
			$this->auth = filter_var_array( $_POST,
				[
					'email' => FILTER_SANITIZE_EMAIL,
					'pass'  => FILTER_SANITIZE_STRING
				]
			);
		}

		if ( $login && $pass ) {
			$this->auth = [
				'email' => filter_var( $login, FILTER_SANITIZE_EMAIL ),
				'pass'  => filter_var( $pass, FILTER_SANITIZE_STRING )
			];
		}

		if ( $this->auth ) {
			$this->session->setSession( $this->auth );

			return true;
		}

		return false;
	}

	/**
	 * @return bool
	 */
	public function isAuth() {
		if ( isset( $_SESSION['auth'] ) ) {
			return true;
		}

		return false;
	}

	/**
	 * Init PHPImap class
	 */
	public function setImap() {

		try {
			if ( $this->isAuth() ) {
				$user       = $this->session->getSession();
				$this->imap = new PhpImap\Mailbox( '{imap.gmail.com:993/imap/ssl}INBOX',
					$user['email'],
					$user['pass']
				);
				$this->imap->checkMailbox();
			}
		} catch ( \Throwable $e ) {
			$this->session->destroySession();
			echo $e->getMessage();
		}
	}

	/**
	 * @return PhpImap\Mailbox
	 */
	public function getImap(): PhpImap\Mailbox {
		return $this->imap;
	}

	public function getCurrentEmail() {
		if ( $this->session ) {
			return $this->session->getSession()['email'];
		}

		return false;
	}

	/**
	 * Set filters from $_GET
	 *
	 * @return bool|mixed
	 */
	private function getCriteriaSearch() {
		if ( ! empty( $_GET ) ) {
			$filters = filter_var_array( $_GET,
				[
					'search' => FILTER_SANITIZE_STRING,
					'sort'   => FILTER_SANITIZE_EMAIL,
					'count'  => FILTER_SANITIZE_NUMBER_INT,
				]
			);

			if ( $filters['search'] ) {
				$filters['search'] = 'BODY "' . $filters['search'] . '"';
			}

			return $filters;
		}

		return false;
	}

	/**
	 * Get emails
	 *
	 * @return array|bool
	 */
	public function getData() {

		$email_ids = $this->imap->searchMailbox( $this->getCriteriaSearch()['search'] );
		if ( $email_ids ) {

			if ( $this->getCriteriaSearch()['sort'] == 'desc' ) {
				$email_ids = array_reverse( $email_ids );
			}

			$email_ids          = array_slice( $email_ids, 0, ( $_GET['count'] ? $_GET['count'] : 5 ) );
			$data['emails_ids'] = implode( ',', $email_ids );

			foreach ( $email_ids as $email_id ) {
				$email = $this->imap->getMail( $email_id );

				$data['data'][] = [
					'id'           => $email->id,
					'from'         => $email->fromAddress,
					'subject'      => $email->subject,
					'date'         => $email->date,
					'message'      => mb_strimwidth( $email->textPlain, 0, 100, '...' ),
					'full_message' => $email->textHtml
				];
			}

			return $data;
		}

		return false;
	}

	public function getEmailsByIds( array $ids ): array {
		foreach ( $ids as $email_id ) {
			$email    = $this->imap->getMail( $email_id );
			$emails[] = [
				'id'           => $email->id,
				'from'         => $email->fromAddress,
				'subject'      => $email->subject,
				'date'         => $email->date,
				'full_message' => $email->textHtml
			];
		}

		return $emails;
	}

	/**
	 * @param int $id
	 */
	public function deleteMessage( int $id ) {
		$this->imap->deleteMail( $id );
	}

	/**
	 * @return string
	 */
	public function getSiteUrl() {
		return sprintf(
			"%s://%s",
			isset( $_SERVER['HTTPS'] ) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
			$_SERVER['SERVER_NAME']
		);
	}

	/**
	 * User logout
	 */
	public function logOut() {
		$this->session->destroySession();
		header( 'Location: ' . $this->getSiteUrl() );
	}
}


