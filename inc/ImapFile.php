<?php namespace Imap;

/**
 * Class ImapFile
 * @package Imap
 */
class ImapFile {

	const FILE = '_emails.json';

	/**
	 * @param array $data
	 *
	 * @return bool
	 */
	public function writeFile( array $data ): bool {
		try {
			if ( file_put_contents( $this->getIp() . self::FILE, json_encode( $data, JSON_UNESCAPED_UNICODE ) ) ) {
				return true;
			}

			throw new \Error( 'file error' );

		} catch ( \Exception $e ) {
			echo 'Caught exception: ', $e->getMessage(), "\n";
		}

		return false;
	}

	/**
	 * @return string
	 */
	function getIp() {
		$ipaddress = true;
		if ( isset( $_SERVER['HTTP_CLIENT_IP'] ) ) {
			$ipaddress = $_SERVER['HTTP_CLIENT_IP'];
		} else if ( isset( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else if ( isset( $_SERVER['HTTP_X_FORWARDED'] ) ) {
			$ipaddress = $_SERVER['HTTP_X_FORWARDED'];
		} else if ( isset( $_SERVER['HTTP_FORWARDED_FOR'] ) ) {
			$ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
		} else if ( isset( $_SERVER['HTTP_FORWARDED'] ) ) {
			$ipaddress = $_SERVER['HTTP_FORWARDED'];
		} else if ( isset( $_SERVER['REMOTE_ADDR'] ) ) {
			$ipaddress = $_SERVER['REMOTE_ADDR'];
		} else {
			$ipaddress = false;
		}

		return $ipaddress;
	}
}

