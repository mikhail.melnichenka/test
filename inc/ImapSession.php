<?php namespace Imap;

/**
 * Class ImapSession
 * @package Imap
 */
class ImapSession {

	/**
	 * ImapSession constructor.
	 */
	public function __construct() {
		session_start();
	}

	/**
	 * @param $data
	 */
	public function setSession( $data ) {
		$_SESSION['auth'] = [
			'email' => $data['email'],
			'pass'  => $data['pass']
		];
	}

	/**
	 * @return mixed
	 */
	public function getSession() {
		if ( $_SESSION['auth'] ) {
			return $_SESSION['auth'];
		}

		return false;
	}

	/**
	 * To destroy current session
	 */
	public function destroySession() {
		unset( $_SESSION['auth'] );
		session_destroy();
	}
}